import { useBox, usePlane } from '@react-three/cannon'

import React, { useRef } from 'react'
import { useGLTF } from '@react-three/drei'
import { GLTF } from 'three/examples/jsm/loaders/GLTFLoader'

// @ts-ignore
export function Ground(props) {
  const [ref] = usePlane(() => ({ type: 'Static', position: [0, 0, 0], rotation: [-Math.PI / 2, 0, 0], ...props }))
  return (
    <mesh ref={ref} receiveShadow>
      <boxBufferGeometry args={[1000, 1000]} />
      <meshBasicMaterial color="#4529ff" />
    </mesh>
  )
}

type GLTFResult = GLTF & {
  nodes: {
    Plane: THREE.Mesh
  }
  materials: {}
}

export default function Stage(props: JSX.IntrinsicElements['group']) {
  const group = useRef<THREE.Group>()
  const { nodes, materials } = useGLTF('/models/stage.glb') as GLTFResult

  return (
    <group ref={group} {...props} scale={1} dispose={null}>
      <mesh castShadow receiveShadow geometry={nodes.Plane.geometry} material={nodes.Plane.material} userData={{ name: 'Plane' }} />
    </group>
  )
}

useGLTF.preload('/models/stage.glb')
