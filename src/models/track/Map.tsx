/*
Auto-generated by: https://github.com/pmndrs/gltfjsx
*/

import * as THREE from 'three'
import React, { useLayoutEffect, useRef } from 'react'
import { useFrame } from '@react-three/fiber'
import { MeshDistortMaterial, PositionalAudio, useGLTF } from '@react-three/drei'
import type { GLTF } from 'three-stdlib'
import type { GroupProps } from '@react-three/fiber'

import { useStore, levelLayer } from '../../store'
import { useToggle } from '../../useToggle'

type GLTFResult = GLTF & {
  nodes: {
    city_sample_canals_city_mat001_0: THREE.Mesh
    city_sample_earth_city_mat_0: THREE.Mesh
    city_sample_floor_sidewalk_mat_0: THREE.Mesh
    city_sample_houses_1_city_mat002_0: THREE.Mesh
    city_sample_houses_1_city_mat002_0001: THREE.Mesh
    city_sample_houses_1_city_mat002_0002: THREE.Mesh
    city_sample_houses_1_city_mat002_0003: THREE.Mesh
    city_sample_houses_2_city_mat003_0: THREE.Mesh
    city_sample_houses_2_city_mat003_0001: THREE.Mesh
    city_sample_houses_2_city_mat003_0002: THREE.Mesh
    city_sample_houses_2_city_mat003_0003: THREE.Mesh
    city_sample_houses_2_city_mat003_0004: THREE.Mesh
    city_sample_houses_3_city_mat004_0: THREE.Mesh
    city_sample_houses_3_city_mat004_0001: THREE.Mesh
    city_sample_streets_city_mat005_0: THREE.Mesh
    city_sample_streets_city_mat005_0001: THREE.Mesh
    city_sample_streets_city_mat005_0002: THREE.Mesh
  }
  materials: {
    ['city_mat.001']: THREE.MeshStandardMaterial
    city_mat: THREE.MeshStandardMaterial
    sidewalk_mat: THREE.MeshStandardMaterial
    ['city_mat.002']: THREE.MeshStandardMaterial
    ['city_mat.003']: THREE.MeshStandardMaterial
    ['city_mat.004']: THREE.MeshStandardMaterial
    ['city_mat.005']: THREE.MeshStandardMaterial
  }
}

export default function Model(props: JSX.IntrinsicElements['group']) {
  const level = useStore((state) => state.level)
  const group = useRef<THREE.Group>()
  const { nodes, materials } = useGLTF('/models/map-draco.glb') as GLTFResult
  const config = { receiveShadow: true, castShadow: true, 'material-roughness': 1 }

  useLayoutEffect(() => void level.current?.traverse((child) => child.layers.enable(levelLayer)), [])

  return (
    <group ref={group} {...props} dispose={null}>
      <group position={[-3.65, -0.42, 7.99]} scale={0.006} userData={{ name: 'City Set - Proto Series' }}>
        <group position={[748.11, 534.37, 650.76]} rotation={[Math.PI, 0.76, 2.68]} scale={[100, 100, 100]} userData={{ name: 'Camera.001' }} />
        <group userData={{ name: 'city_sample_canals' }}>
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_canals_city_mat001_0.geometry}
            material={materials['city_mat.001']}
            userData={{ name: 'city_sample_canals_city_mat.001_0' }}
            {...config}
          />
        </group>
        <group userData={{ name: 'city_sample_earth' }}>
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_earth_city_mat_0.geometry}
            material={materials.city_mat}
            userData={{ name: 'city_sample_earth_city_mat_0' }}
            {...config}
          />
        </group>
        <group userData={{ name: 'city_sample_floor' }}>
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_floor_sidewalk_mat_0.geometry}
            material={materials.sidewalk_mat}
            userData={{ name: 'city_sample_floor_sidewalk_mat_0' }}
            {...config}
          />
        </group>
        <group userData={{ name: 'city_sample_houses_1' }}>
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_houses_1_city_mat002_0.geometry}
            material={nodes.city_sample_houses_1_city_mat002_0.material}
            userData={{ name: 'city_sample_houses_1_city_mat.002_0' }}
            {...config}
          />
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_houses_1_city_mat002_0001.geometry}
            material={nodes.city_sample_houses_1_city_mat002_0001.material}
            userData={{ name: 'city_sample_houses_1_city_mat.002_0.001' }}
            {...config}
          />
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_houses_1_city_mat002_0002.geometry}
            material={nodes.city_sample_houses_1_city_mat002_0002.material}
            userData={{ name: 'city_sample_houses_1_city_mat.002_0.002' }}
            {...config}
          />
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_houses_1_city_mat002_0003.geometry}
            material={nodes.city_sample_houses_1_city_mat002_0003.material}
            userData={{ name: 'city_sample_houses_1_city_mat.002_0.003' }}
            {...config}
          />
        </group>
        <group userData={{ name: 'city_sample_houses_2' }}>
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_houses_2_city_mat003_0.geometry}
            material={nodes.city_sample_houses_2_city_mat003_0.material}
            userData={{ name: 'city_sample_houses_2_city_mat.003_0' }}
            {...config}
          />
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_houses_2_city_mat003_0001.geometry}
            material={nodes.city_sample_houses_2_city_mat003_0001.material}
            userData={{ name: 'city_sample_houses_2_city_mat.003_0.001' }}
            {...config}
          />
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_houses_2_city_mat003_0002.geometry}
            material={nodes.city_sample_houses_2_city_mat003_0002.material}
            userData={{ name: 'city_sample_houses_2_city_mat.003_0.002' }}
            {...config}
          />
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_houses_2_city_mat003_0003.geometry}
            material={nodes.city_sample_houses_2_city_mat003_0003.material}
            userData={{ name: 'city_sample_houses_2_city_mat.003_0.003' }}
            {...config}
          />
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_houses_2_city_mat003_0004.geometry}
            material={nodes.city_sample_houses_2_city_mat003_0004.material}
            userData={{ name: 'city_sample_houses_2_city_mat.003_0.004' }}
            {...config}
          />
        </group>
        <group userData={{ name: 'city_sample_houses_3' }}>
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_houses_3_city_mat004_0.geometry}
            material={nodes.city_sample_houses_3_city_mat004_0.material}
            userData={{ name: 'city_sample_houses_3_city_mat.004_0' }}
            {...config}
          />
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_houses_3_city_mat004_0001.geometry}
            material={nodes.city_sample_houses_3_city_mat004_0001.material}
            userData={{ name: 'city_sample_houses_3_city_mat.004_0.001' }}
            {...config}
          />
        </group>
        <group userData={{ name: 'city_sample_streets' }}>
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_streets_city_mat005_0.geometry}
            material={nodes.city_sample_streets_city_mat005_0.material}
            userData={{ name: 'city_sample_streets_city_mat.005_0' }}
            {...config}
          />
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_streets_city_mat005_0001.geometry}
            material={nodes.city_sample_streets_city_mat005_0001.material}
            userData={{ name: 'city_sample_streets_city_mat.005_0.001' }}
            {...config}
          />
          <mesh
            castShadow
            receiveShadow
            geometry={nodes.city_sample_streets_city_mat005_0002.geometry}
            material={nodes.city_sample_streets_city_mat005_0002.material}
            userData={{ name: 'city_sample_streets_city_mat.005_0.002' }}
            {...config}
          />
        </group>
        <group position={[407.62, 590.39, -100.55]} rotation={[1.89, 0.88, -2.05]} scale={[100, 100, 100]} userData={{ name: 'Lamp' }}>
          <group rotation={[Math.PI / 2, 0, 0]} userData={{ name: 'Node_29' }} />
        </group>
      </group>
    </group>
  )
}
